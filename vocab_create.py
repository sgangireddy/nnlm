
class Vocabulary(object):
    
    def __init__(self, path_name): #constructor
        
        self.path_name = path_name
        self.vocab = {}
        self.short_list = {}
        self.short_list_freq = 0
        self.vocab_size = 0
        self.short_list_size = 0 
       	self.count = 0 

    def vocab_create(self):
        f = open (self.path_name, 'r')
        for line in f:
            line = line.strip().split()
	    self.count = self.count + 1
            for unigram in line:
                if self.vocab == {}:
                    self.vocab[unigram] = 1
                    self.vocab['<s>'] = 1
                    self.vocab['</s>'] = 1
                elif self.vocab.has_key(unigram):
                    self.vocab[unigram] = self.vocab[unigram] + 1
                else:
                    self.vocab[unigram] = 1
        #self.vocab['<unk>'] =  1
        f.close()
	self.vocab['<s>'], self.vocab['</s>'] = self.count, self.count

	#i=0
	#for unigram in sorted(self.vocab, key=self.vocab.get, reverse=True):
	#    i+=1
	#    if i<2000:
	#    	self.short_list[unigram] = self.vocab[unigram]

	#for unigram in sorted(self.short_list, key=self.short_list.get, reverse=True):
	#    self.short_list[unigram] = self.short_list_size
	#    self.short_list_size += 1

        for unigram in self.vocab.keys():
            if self.vocab[unigram] > 45:
                self.short_list_freq = self.short_list_freq + self.vocab[unigram]
                self.short_list[unigram] = self.short_list_size
                self.short_list_size = self.short_list_size + 1
        for unigram in self.vocab.keys():
            self.vocab[unigram] = self.vocab_size
            self.vocab_size = self.vocab_size + 1
 
