import theano
import theano.tensor as T
import time, numpy
from vocab_create import Vocabulary
from data_provider import DataProvider
import os, sys, getopt
from learn_rates import LearningRateNewBob, LearningRateList, LearningRate
from logistic_regression import LogisticRegression
from mlp_gpu import MLP
from cache import TNetsCacheSimple, TNetsCacheLastElem
from numpy.core.numeric import dtype
import h5py
from mlp_save import save_mlp, save_posteriors, save_learningrate
      
def train_mlp(feature_dimension, context, hidden_size, file_name, momentum, batch_size, L1_reg = 0.0, L2_reg = 0.0001, path_name = '/exports/work/inf_hcrc_cstr_udialogue/siva/data/', num_batches_per_bunch=8):
    
    #path to save tuned weights of neural network
    weight_path = '/exports/work/inf_hcrc_cstr_udialogue/siva/mlp/nnets/'

    #vocabulary
    voc_list = Vocabulary(path_name + 'train2')
    voc_list.vocab_create()
    vocab = voc_list.vocab
    vocab_size = voc_list.vocab_size
    short_list = voc_list.short_list
    short_list_size = voc_list.short_list_size
    
    momentum = float(momentum)
    batch_size = int(batch_size)
    
    #dataproviders for train, valid and test 
    dataprovider_train = DataProvider(path_name + 'train2', vocab, vocab_size)
    dataprovider_valid = DataProvider(path_name + 'valid2', vocab, vocab_size)
    dataprovider_test = DataProvider(path_name + 'test2', vocab, vocab_size)

    print '..building the model'
    #symbolic variables for input, target vector, batch index and learning rate
    index = T.lscalar('index')
    x1 = T.fmatrix('x1')
    x2 = T.fmatrix('x2')
    y = T.ivector('y')
    learning_rate = T.fscalar('learning_rate') 

    #theano shared variables for train, valid and test
    train_set_x1 = theano.shared(numpy.empty((1, 1), dtype='float32'), allow_downcast = True)
    train_set_x2 = theano.shared(numpy.empty((1, 1), dtype='float32'), allow_downcast = True)
    train_set_y = theano.shared(numpy.empty((1), dtype = 'float32'), allow_downcast = True)
    
    valid_set_x1 = theano.shared(numpy.empty((1, 1), dtype='float32'), allow_downcast = True)
    valid_set_x2 = theano.shared(numpy.empty((1, 1), dtype='float32'), allow_downcast = True)
    valid_set_y = theano.shared(numpy.empty((1), dtype = 'float32'), allow_downcast = True)
    
    test_set_x1 = theano.shared(numpy.empty((1, 1), dtype='float32'), allow_downcast = True)
    test_set_x2 = theano.shared(numpy.empty((1, 1), dtype='float32'), allow_downcast = True)
    test_set_y = theano.shared(numpy.empty((1), dtype = 'float32'), allow_downcast = True)
    
    rng = numpy.random.RandomState() 
   
    classifier = MLP(rng = rng, input1 = x1, input2 = x2, n_in = vocab_size, fea_dim = int(feature_dimension), context_size = int(context), n_hidden =int(hidden_size), n_out = vocab_size)

    #obejective function to optimize
    cost = classifier.negative_log_likelihood(y) + L1_reg * classifier.L1 + L2_reg * classifier.L2_sqr 
    
    #constructor for learning rate class
    learnrate_schedular = LearningRateNewBob(start_rate=0.005, scale_by=.5, max_epochs=9999,\
                                    min_derror_ramp_start=.01, min_derror_stop=.01, init_error=100.)

    #provides log likelihood OR sum of log likelihoods for mini batches
    log_likelihood = classifier.sum(y)
    
    #test_model
    test_model = theano.function(inputs = [index], outputs = [log_likelihood],  \
                                 givens = {x1: test_set_x1[index*batch_size:(index + 1)*batch_size],
					   x2: test_set_x2[index*batch_size:(index + 1)*batch_size],
                                           y: T.cast(test_set_y[index*batch_size:(index+1)*batch_size], dtype='int32')})
    #validation_model
    validate_model = theano.function(inputs = [index], outputs = [log_likelihood], \
                                     givens = {x1: valid_set_x1[index*batch_size:(index+1)*batch_size],
					       x2: valid_set_x2[index*batch_size:(index+1)*batch_size],
                                               y: T.cast(valid_set_y[index*batch_size:(index+1)*batch_size], dtype='int32')})

    gradient_param = []
    #calculates the gradient of cost with respect to parameters 
    for param in classifier.params:
        gradient_param.append(T.cast(T.grad(cost, param), 'float32'))
        
    updates = {}
    #updates the parameters
    for dparam, gradient in zip(classifier.delta_params, gradient_param):
    	updates[dparam] = T.cast((momentum * dparam - learning_rate * gradient), dtype='float32')
    for dparam, param in zip(classifier.delta_params, classifier.params):
    #for param, gradient in zip(classifier.params, gradient_param):
    #    updates.append((param, param - learning_rate * gradient))
	updates[param] = param + updates[dparam]
    
    #training_model
    train_model = theano.function(inputs = [index, learning_rate], outputs = [], updates = updates, \
                                 givens = {x1: train_set_x1[index*batch_size:(index+1)*batch_size],
					   x2: train_set_x2[index*batch_size:(index+1)*batch_size],
                                           y: T.cast(train_set_y[index*batch_size:(index+1)*batch_size], dtype='int32')})

    classifier_name = 'MLP' + str(model_number)

    f = h5py.File(weight_path+file_name1, "r")
    for i in xrange(0, classifier.no_of_params, 2):
        path_modified = '/' + classifier_name + '/layer' + str(i/2)
        if i == 4:
           classifier.params[i].set_value(numpy.asarray(f[path_modified + "/W"].value, dtype = 'float32'), borrow = True)
        else:
           classifier.params[i].set_value(numpy.asarray(f[path_modified + "/W"].value, dtype = 'float32'), borrow = True)
           classifier.params[i + 1].set_value(numpy.asarray(f[path_modified + "/b"].value, dtype = 'float32'), borrow = True)
    f.close()


    print '.....training'
    best_valid_loss = numpy.inf
    start_time = time.time()
    while(learnrate_schedular.get_rate() != 0):
    
        print 'learning_rate:', learnrate_schedular.get_rate()		
        print 'epoch_number:', learnrate_schedular.epoch        
        frames_showed, progress = 0, 0
        start_epoch_time = time.time()
	
	dataprovider_train.reset()
	tqueue = TNetsCacheSimple.make_queue() #intiation of caching obeject to queue the data
        cache = TNetsCacheSimple(tqueue, shuffle_frames = True, offset=0, \
                                 batch_size = batch_size, num_batches_per_bunch = num_batches_per_bunch, vocab_size=vocab_size)
	cache.data_provider = dataprovider_train
	cache.start() #separate thread
        
	while True:
	    feats_lab_tuple = TNetsCacheSimple.get_elem_from_queue(tqueue) #bunch of training examples
            if isinstance(feats_lab_tuple, TNetsCacheLastElem):
                break
	
	    features, labels = feats_lab_tuple
            frames_showed += features.shape[0]
	    train_batches = features.shape[0]/batch_size

            if(features.shape[0] % batch_size!=0 or features.shape[0] < batch_size):
                train_batches += 1
	    
            train_set_x1.set_value(features[:,0:vocab_size], borrow = True) #features of word1
            train_set_x2.set_value(features[:,vocab_size:2*vocab_size], borrow = True) #features of word2
            train_set_y.set_value(numpy.asarray(labels.flatten(), dtype = 'float32'), borrow = True) #labels
	    for i in xrange(train_batches): #iteartes over number of bunches, each bunch consists of minibatch
            	train_model(i, numpy.array(learnrate_schedular.get_rate(), dtype = 'float32'))
            
	    progress += 1
            if progress%100000==0:
                end_time_progress = time.time()
                print 'PROGRESS: Processed %i bunches (%i frames), TIME: %f in seconds'\
                          %(progress, frames_showed,(end_time_progress-start_epoch_time))
        
        end_time_progress = time.time()
        print 'PROGRESS: Processed %i bunches (%i frames), TIME: %f in seconds'\
                          %(progress, frames_showed,(end_time_progress-start_epoch_time))
        classifier_name = 'MLP' + str(learnrate_schedular.epoch)
        save_mlp(classifier, weight_path+file_name, classifier_name) #saves the weights after each epoch
    
        print 'Validating...'
        log_likelihood = []
        valid_frames_showed, progress = 0, 0
        start_valid_time = time.time() 

        dataprovider_valid.reset()        
        tqueue= TNetsCacheSimple.make_queue()
        cache= TNetsCacheSimple(tqueue, offset=0, num_batches_per_bunch = 16, vocab_size=vocab_size)
        cache.data_provider = dataprovider_valid
        cache.start()

        while True:
            feats_lab_tuple = TNetsCacheSimple.get_elem_from_queue(tqueue)
            if isinstance(feats_lab_tuple, TNetsCacheLastElem):
                break

	    features, labels = feats_lab_tuple

            valid_frames_showed += features.shape[0]
            valid_batches = features.shape[0]/batch_size

            if(features.shape[0] % batch_size!=0 or features.shape[0] < batch_size):
                valid_batches += 1

            valid_set_x1.set_value(features[:,0:vocab_size], borrow = True)
            valid_set_x2.set_value(features[:,vocab_size:2*vocab_size], borrow = True)
            valid_set_y.set_value(numpy.asarray(labels.flatten(), dtype = 'float32'), borrow = True)
            for i in xrange(valid_batches):
                log_likelihood.append(validate_model(i))

            progress += 1
            if progress%10000==0:
                end_time_valid_progress = time.time()
                print 'PROGRESS: Processed %i bunches (%i frames),  TIME: %f in seconds'\
                          %(progress, valid_frames_showed, end_time_valid_progress - start_valid_time)
        
        end_time_valid_progress = time.time()
        print 'PROGRESS: Processed %i bunches (%i frames),  TIME: %f in seconds'\
                          %(progress, valid_frames_showed, end_time_valid_progress - start_valid_time)           

	#computes entropy on validation data 
        entropy = (-numpy.sum(log_likelihood)/valid_frames_showed)
        print entropy, numpy.sum(log_likelihood)
        
	if entropy < best_valid_loss:
           learning_rate = learnrate_schedular.get_next_rate(entropy)
           best_valid_loss = entropy
        else:
           learnrate_schedular.rate = 0.0
    end_time = time.time()
    print 'The fine tuning ran for %.2fm' %((end_time-start_time)/60.)
    
    print 'Testing...'
    log_likelihood = []
    test_frames_showed, progress = 0, 0
    start_test_time = time.time() # it is also stop of training time
    
    dataprovider_test.reset()
    tqueue = TNetsCacheSimple.make_queue()
    cache = TNetsCacheSimple(tqueue, offset=0, num_batches_per_bunch = 16, vocab_size=vocab_size)
    cache.data_provider = dataprovider_test
    cache.start()

    while True:
        feats_lab_tuple = TNetsCacheSimple.get_elem_from_queue(tqueue)
        if isinstance(feats_lab_tuple, TNetsCacheLastElem):
           break

	features, labels = feats_lab_tuple
#        features1, features2, labels = get_features(feats_lab_tuple, vocab_size)

        test_frames_showed += features.shape[0]
        test_batches = features.shape[0]/batch_size

        if(features.shape[0] % batch_size!=0 or features.shape[0] < batch_size):
           test_batches += 1

        test_set_x1.set_value(features[:,0:vocab_size], borrow = True)
        test_set_x2.set_value(features[:,vocab_size:2*vocab_size], borrow = True)
        test_set_y.set_value(numpy.asarray(labels.flatten(), dtype = 'float32'), borrow = True)
        for i in xrange(test_batches):
            log_likelihood.append(test_model(i))
 
        progress += 1
        if progress%10000==0:
           end_time_test_progress = time.time()
           print 'PROGRESS: Processed %i bunches (%i frames),  TIME: %f in seconds'\
                          %(progress, test_frames_showed, end_time_test_progress - start_test_time)
    end_time_test_progress = time.time()
    print 'PROGRESS: Processed %i bunches (%i frames),  TIME: %f in seconds'\
                    %(progress, test_frames_showed, end_time_test_progress - start_test_time)           
 
    print numpy.sum(log_likelihood)
    likelihood_sum = (-numpy.sum(log_likelihood)/test_frames_showed)
    print 'entropy:', likelihood_sum

opts, extraparams = getopt.getopt(sys.argv[1:], "f:c:h:m:M:b:", ["--feature", "--context", "--hidden", "--file_name", '--momentum', "--batch"])
for o,p in opts:
  if o in ['-f','--feature']:
     feature_dimension = p
  elif o in ['-c', '--context']:
     context = p
  elif o in ['-h', '--hidden']:
     hidden_size = p
  elif o in ['-m', '--file_name']:
     file_name = p
  elif o in ['-M', '--momentum']:
     momentum = p
  elif o in ['-b', '--batch']:
     batch_size = p

train_mlp(feature_dimension, context, hidden_size, file_name, momentum, batch_size)
